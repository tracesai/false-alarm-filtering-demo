import os
import time

import cv2
import json
import requests
from typing import Union


def get_paths_to_files_in_dir(dirname: str, expected_filetype=None):
    """
    Create a list of objects stored in the dirname with paths
    :param dirname:
    :param expected_filetype: check if all stored objects are one of the expected types
    :return:
    """
    if expected_filetype is None:
        output = [os.path.join(dirname, x) for x in os.listdir(dirname)]
    elif isinstance(expected_filetype, list):
        output = [os.path.join(dirname, x) for x in os.listdir(dirname) if os.path.splitext(x)[-1] in expected_filetype]
    else:
        raise TypeError("Incorrect expected_filetype provided. The expected_filetype should be a list.")
    return output


class FilesExistError(Exception):
    pass


def extract_frames_from_video(path_to_video: str, extracted_frames_folder: str, target_fps: Union[int, float] = 0.5):
    """
    Extracts frames from a given video with target frame per rate ratio.
    The default target_fps is 0.5, meaning 1 frame per 2 seconds is extracted.
    If a video is 5 seconds long or shorter, target_fps is 2, meaning 2 frames per second are extracted,
    If a video is longer than 5 seconds, but shorter than 10 seconds, target_fps is 1,
    meaning 1 frame per second is extracted.
    These settings are adjustable and can be set individually for different use-cases.

    :param path_to_video:
    :param extracted_frames_folder:
    :param target_fps:
    :return:
    """
    print(f"Reading {path_to_video}")
    # Open the video file
    video = cv2.VideoCapture(path_to_video)

    # Get the total number of frames in the video
    total_frames = int(video.get(cv2.CAP_PROP_FRAME_COUNT))

    # Get the video's fps
    video_fps = video.get(cv2.CAP_PROP_FPS)
    if video_fps == 0:
        raise Exception("Failed to extract frames from video. The video file is corrupted.")

    if 5 < int(total_frames / video_fps) < 10:  # if duration of a video is less than 10 seconds, fps set to 1
        target_fps = 1
    elif int(total_frames / video_fps) < 5:  # if duration of a video is less than 5 seconds, fps set to 2
        target_fps = 2

    # Calculate the desired frame interval based on the frame rate
    frame_interval = int(video_fps / target_fps)

    if not os.path.exists(extracted_frames_folder):
        os.makedirs(extracted_frames_folder)
    else:
        raise FilesExistError("Folder with extracted frames already exists.")

    # Start reading the frames
    success, frame = video.read()
    current_frame = 0
    frame_counter = 0

    while success:
        if frame_counter % frame_interval == 0 and current_frame < 30:
            # Save the frame as an image
            cv2.imwrite(os.path.join(extracted_frames_folder,
                                     "frame_{}.jpg".format(current_frame)), frame)
            current_frame += 1
        elif current_frame >= 30:
            print("The video is too long. Only first minute of a video is analysed")
            break

        success, frame = video.read()
        frame_counter += 1

    if current_frame < 5:
        raise Exception("Too few frames were extracted. The provided video is too short.")

    # Release the video file
    video.release()

    print(f"Finished extracting frames for video {os.path.basename(path_to_video)}")
    return


def read_frames(path_to_frames: str) -> dict[str, tuple[str, bytes, str]]:
    """
    Read frames stored in path_to_frames
    :param path_to_frames: a path to previously extracted frames
    :return: dict with frames
    """
    frames = {}
    all_frames_paths = get_paths_to_files_in_dir(path_to_frames, expected_filetype=['.jpg', '.png'])
    all_frames_paths = sorted(all_frames_paths, key=lambda x: int(os.path.splitext(x)[0].split("_")[-1]))
    for (i, img) in enumerate(all_frames_paths):
        with open(img, "rb") as f:
            im_bytes = f.read()
        frames['image_{}'.format(i)] = ('image_{}.jpg'.format(i), im_bytes, 'image/jpeg')
    return frames


def save_prediction(data_to_save, path_to_save, filename: str = "traces_prediction.json"):
    """
    Save data as json file in path_to_save with a given filename. Default: "traces_prediction.json"
    :param data_to_save:
    :param path_to_save:
    :param filename:
    :return:
    """
    path_to_save_predictions = os.path.join(path_to_save, filename)
    print(f"Saving predictions to {path_to_save_predictions}")
    with open(path_to_save_predictions, 'w') as outfile:
        json.dump(data_to_save, outfile, indent=4)
    return


def send_request(extracted_frames_folder, url, key):
    headers = {
        'x-api-key': key
    }
    metadata = {'metadata': json.dumps({
        'camera_id': 'b5271b30-aes1',  # for demo purposes
        'event_id': 'ddf2-fo52-vj52',  # for demo purposes
        'timestamp': 1585840725})  # for demo purposes
    }

    files: dict = read_frames(path_to_frames=extracted_frames_folder)

    print(f"Sending request with {len(files.items())} frames to Traces AI")

    output = requests.post(url, data=metadata, headers=headers, files=files)

    if output.status_code == 401:
        print(json.loads(output.content.decode('utf-8'))['description'], "Make sure you provided correct key")
    elif output.status_code == 400:
        print(output.content.decode('utf-8'), "Too few frames were extracted from a video. The video is too short.")
    elif output.status_code == 500:
        raise ValueError("Unexpected error")
    elif output.status_code == 200:
        save_prediction(output.json(), path_to_save=extracted_frames_folder)
