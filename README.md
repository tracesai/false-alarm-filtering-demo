# Instructions for Demo
This README provides instructions on how to run the demo. 

Note:
- Please make sure you have Python 3.11 or newer installed on your machine. 
- The instructions provided below are for macOS and Linux systems. However, the steps might be slightly different if you are using a different operating system.

## Step 1: Download the Repository
To get started, download the current repository to your local machine. Open your terminal and run the following command:

```bash
git clone https://gitlab.com/tracesai/false-alarm-filtering-demo.git
```

## Step 2: Navigate to the Repository
In the terminal, navigate to the cloned repository on your local machine using the following command:

```bash
cd /path/to/local/repository
```
Replace `/path/to/local/repository` with the actual path to the directory where the repository was cloned. 
By default, the command `cd false-alarm-filtering-demo` should work

## Step 3: Set up the Virtual Environment and Install Dependencies
Before running the demo, you need to create a virtual environment and install the required dependencies. 

1. Create a virtual environment by running the following commands:
```bash
python3 -m venv venv
source venv/bin/activate
```

2. Install the required dependencies with the following command:
```bash
pip3 install -r requirements.txt
```

## Step 4: Prepare the Test Data
To test the demo, you need to prepare a directory with videos. Follow these steps:

1. Create a directory within the repository called `test_data`. Inside the `test_data` directory, create a folder called `videos`.  
Use the following command to create both directories:
```bash
mkdir -p test_data/videos
```

2. Store the videos you want to test within the folder test_data/videos.

3. Verify that the `videos` directory contains the desired videos by running the following command:

```bash
ls -l test_data/videos | wc -l
```
The above command will display the number of files in the `test_data/videos` directory.

## Step 5: Run the Python Script
Finally, you can run the demo by executing the following command:

```bash
python3 main.py --key "INSERT_YOUR_PRIVATE_KEY_HERE" --url "INSERT_URL_HERE"
```
Replace `"INSERT_YOUR_PRIVATE_KEY_HERE"` with your actual private key, and `"INSERT_URL_HERE"` with the shared URL.

Make sure to keep the virtual environment activated (`source venv/bin/activate`) before running the script.

That's it! You are now ready to start the demo. Feel free to reach out if you have any questions or encounter any issues.


## Overview of the steps performed by the program
The code in this repository performs the following tasks:

1. First, it creates a folder named `test_data/frames` to store the extracted frames and predicted outputs for each video.

2. For each video in the `test_data/videos` folder, the program extracts frames and creates a corresponding folder in `test_data/frames`. For example, if there are two videos named "video_0.mp4" and "video_1.mp4" in the `test_data/videos` folder, the program will create two folders named "video_0" and "video_1" in the `test_data/frames` folder. Each of these folders will contain the extracted frames from the corresponding video.

3. After the frames extraction step, the program sends these frames to an API for False Alarm Filtering. The metadata, including the camera_id, event_id, and timestamp, is hardcoded for demonstration purposes. 

4. Once a response is received from the API, it is saved in the file `test_data/frames/<video_name>/traces_prediction.json`, where `<video_name>` is the name of the video for which the frames were extracted.

5. Once the execution is finished, you can navigate to the `test_data/frames` folder to check the results. Inside this folder, you will find subfolders for each video that was processed. Each subfolder will contain the extracted frames as well as the `traces_prediction.json` file, which contains the predicted outputs from the API.

During the execution of the program, intermediate information and steps are displayed on the console for monitoring purposes.