import argparse
import os
from helpers import extract_frames_from_video, send_request, get_paths_to_files_in_dir, FilesExistError


def main(arguments):
    all_videos_paths = get_paths_to_files_in_dir(arguments.folder_with_videos, expected_filetype=[".mp4"])
    if not all_videos_paths:
        raise ValueError(f"No videos found at {arguments.folder_with_videos}. "
                         f"Please make sure that the directory exists "
                         f"and contains at least one video file for future testing.")

    for video_path in all_videos_paths:
        video_name = os.path.splitext(os.path.basename(video_path))[0]
        print(f"Working with {video_name}")
        extracted_frames_folder = os.path.join(arguments.folder_with_extracted_frames, video_name)
        try:
            extract_frames_from_video(video_path, extracted_frames_folder)
        except FilesExistError as error:
            print(error)
            pass
        except Exception as error:
            print(error)
            continue
        send_request(extracted_frames_folder,
                     url=args.url,
                     key=args.key)
        print()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-p", "--folder_with_videos", default="./test_data/videos",
                        type=str, help="Path to a folder with videos")
    parser.add_argument("-f", "--folder_with_extracted_frames", default="./test_data/frames",
                        type=str, help="Path to a folder with extracted frames")
    parser.add_argument("-k", "--key", default="", type=str, required=True, help="Your private key")
    parser.add_argument("-u", "--url", default="", type=str, required=True, help="Url")
    args = parser.parse_args()
    main(args)
